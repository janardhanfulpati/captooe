
jQuery(document).ready(function(){
    /*product*/
        $('#pills-first a').hover(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });
    /*End of product1*/
    /*star*/
     $(".demostar").stars({ text: ["Bad", "Not so bad", "Average", "Good", "Perfect"] });
    /*End of star*/

    $(".dropdown").hover(
        function() { $('.dropdown-menu', this).stop().fadeIn("fast");
        },
        function() { $('.dropdown-menu', this).stop().fadeOut("fast");
    });

    /*Filter accordian*/
        $(document).on('show','.accordion', function (e) {
             //$('.accordion-heading i').toggleClass(' ');
             $(e.target).prev('.accordion-heading').addClass('accordion-opened');
        });
        
        $(document).on('hide','.accordion', function (e) {
            $(this).find('.accordion-heading').not($(e.target)).removeClass('accordion-opened');
            //$('.accordion-heading i').toggleClass('fa-chevron-right fa-chevron-down');
        });
    /*End of Filter accordion*/

    /*range slider*/
        var slider = document.getElementById("myRange");
        var output = document.getElementById("myRangedemo");
        output.innerHTML = slider.value;

        slider.oninput = function() {
          output.innerHTML = this.value;
        }
    /*End of range slider*/
    /*star ratings*/
    $(".demostar").stars({ text: ["Bad", "Not so bad", "hmmm", "Good", "Perfect"] });
            
            $(".more-stars").stars({ stars:20 });
            $(".font-size").stars();
            $(".value-set").stars({ value:4 });
            $(".green-color").stars({ color:'#73AD21' });
            $(".icon-change").stars({
                emptyIcon: 'fa-thumbs-o-up',
                filledIcon: 'fa-thumbs-up'
            });
            $(".text").stars({ 
                text: ["1 star", "2 star", "3 star", "4 star", "5 star"]
            });
            $(".click-callback").stars({ 
                click: function(i) {
                    alert("Star " + i + " selected.");
                }
            });
    /*End of star ratings*/
});

function openNav() {
    document.getElementById("mySidenav").style.width = "70%";
    // document.getElementById("flipkart-navbar").style.width = "50%";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.body.style.backgroundColor = "rgba(0,0,0,0)";
}

/*fix header*/
$(document).ready(function() {
    $('#fix-head').affix({
        offset: {
            top: $('#hide-head').height()
        }
    }); 
});
/*End of fix header*/

/*shop by style slider*/
$(document).ready(function() {
    var owl = $('#shop-by-style');
    owl.owlCarousel({
    margin: 10,
    nav: true,
    loop: true,
    responsive: {
        0: {
            items: 5
            },
        350: {
            items: 6
            },
        600: {
            items: 8
            },
        768: {
            items: 6
            },    
        1000: {
             items: 6
            }
        }
    })
})

/*shop by style slider*/

/*home products slider*/

$(document).ready(function() {
    var owl = $('#home_products1, #home_products2, #home_products3');
    owl.owlCarousel({
    margin: 0,
    nav: true,
    loop: true,
    responsive: {
        0: {
            items: 2
            },
        600: {
            items: 3
            },
        1000: {
             items: 4
            },
        1440: {
             items: 5
            }
        }
    })
})

/*home products slider*/

/*glory section*/
	var slideIndex = 0;
	showDivs(slideIndex);

	function plusDivs(n) {
	  showDivs(slideIndex += n);
	}

	function currentDiv(n) {
	  showDivs(slideIndex = n);
	}

	function showDivs(n) {
	  var i;
	  var x = document.getElementsByClassName("mySlides");
	  var dots = document.getElementsByClassName("demo");
	   if (n > x.length) {slideIndex = 1}
	   if (n < 1) {slideIndex = x.length}
	   for (i = 0; i < x.length; i++) {
		   x[i].style.display = "none";
	   }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" w3-opacity-off", "");
        }
        if(x[slideIndex-1]){  
          x[slideIndex-1].style.display = "block";
          dots[slideIndex-1].className += " w3-opacity-off";
        }
	}
/*glory section*/



/*tab reviews*/
$(document).ready(function(){
getAccordion("#pills-first",600);
});
/* accordion.js */

function getAccordion(element_id,screen) 
{
    $(window).resize(function () { location.reload(); });

    if ($(window).width() < screen) 
    {
        var concat = '';
        obj_tabs = $( element_id + " li" ).toArray();
        obj_cont = $( ".tab-content .tab-pane" ).toArray();
        jQuery.each( obj_tabs, function( n, val ) 
        {
            concat += '<div id="' + n + '" class="panel panel-default panel-default-product">';
            concat += '<div class="panel-heading panel-heading-product" role="tab" id="heading' + n + '">';
            concat += '<h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse' + n + '" aria-expanded="false" aria-controls="collapse' + n + '">' + val.innerText + '</a></h4>';
            concat += '</div>';
            concat += '<div id="collapse' + n + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' + n + '">';
            concat += '<div class="panel-body panel-body-product">' + obj_cont[n].innerHTML + '</div>';
            concat += '</div>';
            concat += '</div>';
        });
        $("#accordion").html(concat);
        $("#accordion").find('.panel-collapse:first').addClass("in");
        $("#accordion").find('.panel-title a:first').attr("aria-expanded","true");
        $(element_id).remove();
        $(".tab-content").remove();
    }   
}
/*tab reviews*/


/*back to top*/
$(document).ready(function(){
     currentDiv(1);
	 $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('#back-to-top').tooltip('show');
});
/*back to top*/

